#!/bin/bash

set -ex

. .gitlab-ci/container/container_pre_build.sh

if [[ "$KERNEL_ARCH" = "arm64" ]]; then
    GCC_ARCH="aarch64-linux-gnu"
    DEBIAN_ARCH="arm64"
    DEVICE_TREES="arch/arm64/boot/dts/rockchip/rk3399-gru-kevin.dtb"
    DEVICE_TREES+=" arch/arm64/boot/dts/amlogic/meson-gxl-s805x-libretech-ac.dtb"
    DEVICE_TREES+=" arch/arm64/boot/dts/allwinner/sun50i-h6-pine-h64.dtb"
    DEVICE_TREES+=" arch/arm64/boot/dts/amlogic/meson-gxm-khadas-vim2.dtb"
    DEVICE_TREES+=" arch/arm64/boot/dts/qcom/apq8016-sbc.dtb"
    DEVICE_TREES+=" arch/arm64/boot/dts/qcom/apq8096-db820c.dtb"
    DEVICE_TREES+=" arch/arm64/boot/dts/amlogic/meson-g12b-a311d-khadas-vim3.dtb"
    DEVICE_TREES+=" arch/arm64/boot/dts/mediatek/mt8173-elm-hana.dtb"
    DEVICE_TREES+=" arch/arm64/boot/dts/mediatek/mt8183-kukui-jacuzzi-juniper-sku16.dtb"
# Not yet in mainline
#    DEVICE_TREES+=" arch/arm64/boot/dts/mediatek/mt8192-asurada-spherion-rev2.dtb"
    DEVICE_TREES+=" arch/arm64/boot/dts/qcom/sc7180-trogdor-lazor-limozeen-nots-r5.dtb"
elif [[ "$KERNEL_ARCH" = "arm" ]]; then
    GCC_ARCH="arm-linux-gnueabihf"
    DEBIAN_ARCH="armhf"
    DEVICE_TREES="arch/arm/boot/dts/rk3288-veyron-jaq.dtb"
    DEVICE_TREES+=" arch/arm/boot/dts/sun8i-h3-libretech-all-h3-cc.dtb"
    DEVICE_TREES+=" arch/arm/boot/dts/imx6q-cubox-i.dtb"
else
    GCC_ARCH="x86_64-linux-gnu"
    DEBIAN_ARCH="amd64"
    DEVICE_TREES=""
fi

export ARCH=${KERNEL_ARCH}
export CROSS_COMPILE="${GCC_ARCH}-"

# The kernel doesn't like the gold linker (or the old lld in our debians).
# Sneak in some override symlinks during kernel build until we can update
# debian.
mkdir -p ld-links
for i in /usr/bin/*-ld /usr/bin/ld; do
    i=`basename $i`
    ln -sf /usr/bin/$i.bfd ld-links/$i
done
export PATH=`pwd`/ld-links:$PATH

git config --global user.email "fdo@example.com"
git config --global user.name "freedesktop.org CI"
git am .gitlab-ci/local/patches/*.patch

for opt in $ENABLE_KCONFIGS; do
  echo CONFIG_$opt=y >> .gitlab-ci/container/${KERNEL_ARCH}.config
done
for opt in $DISABLE_KCONFIGS; do
  echo CONFIG_$opt=n >> .gitlab-ci/container/${KERNEL_ARCH}.config
done

./scripts/kconfig/merge_config.sh ${DEFCONFIG} .gitlab-ci/container/${KERNEL_ARCH}.config

make ${KERNEL_IMAGE_NAME}

mkdir -p /lava-files/
for image in ${KERNEL_IMAGE_NAME}; do
    cp arch/${KERNEL_ARCH}/boot/${image} /lava-files/.
done

if [[ -n ${DEVICE_TREES} ]]; then
    make dtbs
    cp ${DEVICE_TREES} /lava-files/.
fi

if [[ ${DEBIAN_ARCH} = "amd64" ]]; then
    make modules
    mkdir -p install/modules/
    INSTALL_MOD_PATH=install/modules/ make modules_install
fi

if [[ ${DEBIAN_ARCH} = "arm64" ]]; then
    make Image.lzma
    mkimage \
        -f auto \
        -A arm \
        -O linux \
        -d arch/arm64/boot/Image.lzma \
        -C lzma\
        -b arch/arm64/boot/dts/qcom/sdm845-cheza-r3.dtb \
        /lava-files/cheza-kernel
    KERNEL_IMAGE_NAME+=" cheza-kernel"

    # Make a gzipped copy of the Image for db410c.
    gzip -k /lava-files/Image
    KERNEL_IMAGE_NAME+=" Image.gz"
fi

. .gitlab-ci/container/container_post_build.sh

if [[ "$UPLOAD_TO_MINIO" = "1" ]]; then
    xz -9 -T${FDO_CI_CONCURRENT:-4} vmlinux - > /lava-files/vmlinux.xz
    ci-fairy minio login --token-file "${CI_JOB_JWT_FILE}"
    FILES_TO_UPLOAD="$KERNEL_IMAGE_NAME vmlinux.xz"

    if [[ -n $DEVICE_TREES ]]; then
        FILES_TO_UPLOAD="$FILES_TO_UPLOAD $(basename -a $DEVICE_TREES)"
    fi

    for f in $FILES_TO_UPLOAD; do
        ci-fairy minio cp /lava-files/$f \
                minio://${PIPELINE_ARTIFACTS_BASE}/${DEBIAN_ARCH}/$f
    done

    # Pass needed files to the test stage
    mkdir -p install
    cp -rf .gitlab-ci/* install/.
    cp -rf install/common install/ci-common

    # Overwrite testlist and results files from drm-ci with the ones in the kernel tree
    cp -v drivers/gpu/drm/*/ci/*.testlist install/local/. || true
    cp -v drivers/gpu/drm/*/ci/*_results.txt install/local/. || true

    MINIO_ARTIFACT_NAME="kernel-files.tar.gz"
    tar -czf $MINIO_ARTIFACT_NAME install
    ci-fairy minio login --token-file "${CI_JOB_JWT_FILE}"
    ci-fairy minio cp ${MINIO_ARTIFACT_NAME} minio://${PIPELINE_ARTIFACTS_BASE}/${DEBIAN_ARCH}/${MINIO_ARTIFACT_NAME}
fi
