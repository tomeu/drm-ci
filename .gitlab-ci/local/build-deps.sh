#!/bin/bash

set -ex

git clone https://gitlab.freedesktop.org/drm/igt-gpu-tools.git -b master
pushd igt-gpu-tools

MESON_OPTIONS="-Doverlay=disabled                    \
               -Dchamelium=disabled                  \
               -Dvalgrind=disabled                   \
               -Dman=enabled                         \
               -Dtests=enabled                       \
               -Drunner=enabled                      \
               -Dlibunwind=enabled                   \
               -Dprefix=/igt/"

# TODO: Remove this once https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1009805 is fixed and libjson-c-dev:armhf is installed in debian/arm-build
if [[ "$DEBIAN_ARCH" = "armhf" ]]; then
    cp /usr/lib/aarch64-linux-gnu/pkgconfig/json-c.pc /usr/lib/arm-linux-gnueabihf/pkgconfig/json-c.pc
    sed -i 's/aarch64-linux-gnu/arm-linux-gnueabihf/g' /usr/lib/arm-linux-gnueabihf/pkgconfig/json-c.pc
    ln -s /usr/lib/arm-linux-gnueabihf/libjson-c.so.5.1.0 /usr/lib/arm-linux-gnueabihf/libjson-c.so
fi

mkdir -p /igt
meson build $MESON_OPTIONS $EXTRA_MESON_ARGS
ninja -C build -j${FDO_CI_CONCURRENT:-4} || ninja -C build -j 1
ninja -C build install

if [[ -e "/lava-files" ]]; then
  cp -rf /igt /lava-files/rootfs-${DEBIAN_ARCH}/.
fi

popd
